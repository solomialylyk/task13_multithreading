package solomia.model;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class withExecutor {

    public void show() throws IOException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            try {
                new Fibonacci().listFibonacci();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        executorService.shutdown();

    }
}
