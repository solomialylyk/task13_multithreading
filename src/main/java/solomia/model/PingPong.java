package solomia.model;

import java.time.LocalDateTime;

public class PingPong {
    private volatile static long A = 0;
    private static Object sync = new Object();

    public void show() {
        Thread t1 = new Thread(() -> {
            synchronized (sync) {
                for (int i = 1; i <= 1000; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                    }
                    A++;
                    sync.notify();
                }
            }
            System.out.println("finish " + Thread.currentThread().getName());
        });
        Thread t2 = new Thread(() -> {
            synchronized (sync) {
                for (int i = 1; i <= 1000; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                    }
                    A++;
                }
                System.out.println("finish " + Thread.currentThread().getName());
            }
        });
        System.out.println(LocalDateTime.now());
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
        }
        System.out.println(LocalDateTime.now());
        System.out.println("A=" + A);
    }

}
