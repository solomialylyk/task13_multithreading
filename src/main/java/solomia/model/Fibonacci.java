package solomia.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class Fibonacci {
    private static Logger log = LogManager.getLogger(Fibonacci.class);
    private int n;

    protected int fib(int index) {
        if (index == 0) {
            return 0;
        } else if (index == 1) {
            return 1;
        } else if (index == 2) {
            return 1;
        } else {
            return fib(index - 1) + fib(index - 2);
        }
    }
    protected void listFibonacci() throws IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in, StandardCharsets.UTF_8));
        n = Integer.parseInt(br.readLine());
        int[] fibbonachi = new int[n];
        for (int i = 0; i < n; i++) {
            int t = fib(i);
            fibbonachi[i] = t;
            log.info(fibbonachi[i] + " ");
        }
    }


}
