package solomia.model;

import java.io.IOException;

public class withThreads {
    public void show() throws IOException {
        Thread thread= new Thread(()-> {
            String nameThread = Thread.currentThread().getName();
            try {
                new Fibonacci().listFibonacci();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }
}
