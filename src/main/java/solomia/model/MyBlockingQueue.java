package solomia.model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MyBlockingQueue {
    BlockingQueue<Integer> queue;
    public  void doBlockingQueue() {
        queue = new ArrayBlockingQueue<>(5);
        Thread first = new Thread(printFirst());
        Thread second = new Thread(printSecond());
        System.out.println("BlockingQueue");
        first.start();
        second.start();
    }
    private Runnable printFirst() {
        return() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //queue.add(i);
                try{
                    queue.put(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Add -> " + i);
            }
        };
    }

    private Runnable printSecond() {
        return() -> {
            Integer value=0;
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //value=queue.remove();
                try {
                    value=queue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Take -> " + value);
            }
        };
    }
}
