package solomia.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyLock {
    private static int countLock;
    public  long lockId;
    private static Object sync = new Object();
    private static Logger log = LogManager.getLogger(MyLock.class);

    private synchronized void lock() {
        if (countLock >= 0) {
            countLock++;
            lockId = Thread.currentThread().getId();
        }
    }

    private synchronized void unlock() {
        if (countLock == 0) {
            throw new IllegalMonitorStateException();
        }
        countLock--;
        if (countLock == 0) {
            notify();
        }
    }

    public void doMyLock() {
        Thread first = new Thread(printFirst());
        Thread second = new Thread(printSecond());
        Thread third = new Thread(printThird());
        first.start();
        second.start();
        third.start();
    }

    private Runnable printFirst() {
        return () -> {
            lock();
            try {

                Thread.sleep(2000);
                log.info("First");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                unlock();
            }

        };
    }

    private Runnable printSecond() {
        return () -> {
            lock();
            try {
                Thread.sleep(2000);
                log.info("Second");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                unlock();
            }

        };
    }

    private Runnable printThird() {
        return () -> {
            lock();
            try {
                Thread.sleep(2000);
                log.info("Third");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                unlock();
            }

        };
    }
}
