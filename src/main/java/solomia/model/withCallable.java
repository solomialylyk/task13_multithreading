package solomia.model;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class withCallable {
    public Callable callable() {
        return()->{
            synchronized(this){
                int sum= new Fibonacci().fib(5);
                System.out.println(sum);
                return sum;
            }

        };
    }
}
