package solomia;

import solomia.model.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;

import static java.util.concurrent.Executors.callable;
import static java.util.concurrent.Executors.newCachedThreadPool;

public class Application {

    public static void main(String[] args) throws IOException {
        // new PingPong().show();
        // new withThreads().show();
        // new withExecutor().show();
        //System.out.println(new withCallable().show());
        //new SleepTimer().randomSleep(3);
        //new MonitorTask().doTask();
        //new MyBlockingQueue().doBlockingQueue();
        new MyLock().doTask();
    }

}
